Doghouse ProductViewShowCategoryTitle
=================

This module allows you to get the category title to display on the product view page.

## Installation

Install using Composer:

`php composer.phar install`

## How to use

In your template file 'app/design/frontend/THEMENAME/default/tempalte/catalog/product/view.phtml'

you will need to add the following:

`if ($_product->getCategory()) {
    echo $_product->getCategory()->getName();
}`


