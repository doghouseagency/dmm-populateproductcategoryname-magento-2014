<?php

class Doghouse_PopulateProductCategoryName_Model_Observer
{
    /**
     * @event catalog_controller_product_init_after.
     *
     * With stock Magento, you can only get the category on a product view page
     * if the URL contains the category, or if `last_visited_category_id` is set in your session
     * (and the product can be shown in that category). With this module you can more reliably
     * (if a category exists which contains the product) get the category.
     */
    public function CheckAndUpdateProductCategory(Varien_Event_Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();

        if ($product->getCategoryId()) {
           return;
        }

        $productCategoryIds = $product->getCategoryIds();
        $categoryId = (int)reset($productCategoryIds);

        if (!$product->canBeShowInCategory($categoryId)) {
            return;
        }

        $category = Mage::getModel('catalog/category')->load($categoryId);
        $product->setCategory($category);
        Mage::register('current_category', $category);

        return $this;
    }
}

